package pzet.pl.githubapi.viewModel;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import pzet.pl.githubapi.data.LocalService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserListViewModel extends ViewModel implements Callback<List<User>> {

    public MutableLiveData<List<User>> users;

    public LiveData<List<User>> getUsers(Integer offset) {
        if (users == null) {
            users = new MutableLiveData<>();
            loadUsers(offset);
        }
        return users;
    }

    public User getUser(int id){
        for(User user : users.getValue()){
            if(user.id != null && user.id.equals(id)){
                return user;
            }
        }
        return null;
    }

    public void loadUsers(Integer offset) {
        LocalService service = new LocalService();
        service.getUsers(offset).enqueue(this);
    }

    @Override
    public void onResponse(Call<List<User>> call, Response<List<User>> response) {
        Log.e("users: ", response.toString());
        if (response != null) {
            if (response.body() != null && !response.body().isEmpty()) {
                List<User> appendableList = users.getValue() == null ? new ArrayList<>() : users.getValue();
                for (User user : response.body()) {
                    if (appendableList.contains(user)) {
                        appendableList.set(appendableList.indexOf(user), user);
                    } else {
                        appendableList.add(user);
                    }
                }
                users.setValue(appendableList);
            }
        }
    }

    @Override
    public void onFailure(Call<List<User>> call, Throwable t) {
        Log.e("failure: ", t.getMessage());
    }
}
