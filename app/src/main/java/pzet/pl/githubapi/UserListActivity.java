package pzet.pl.githubapi;

import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import pzet.pl.githubapi.viewModel.UserListViewModel;
import pzet.pl.githubapi.viewModel.User;

/**
 * An activity representing a list of Users. This activity
 * has different presentations for handset and tablet-size devices. On
 * handsets, the activity presents a list of items, which when touched,
 * lead to a {@link UserDetailActivity} representing
 * item details. On tablets, the activity presents the list of items and
 * item details side-by-side using two vertical panes.
 */
public class UserListActivity extends AppCompatActivity {

    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
    private boolean mTwoPane;
    private boolean mLoading;

    private RecyclerView recyclerView;
    private SimpleItemRecyclerViewAdapter adapter;
    private UserListViewModel model;
    private final LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);

    private final RecyclerView.OnScrollListener recyclerScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);

            int totalItem = mLayoutManager.getItemCount();
            int lastVisibleItem = mLayoutManager.findLastVisibleItemPosition();

            if (!mLoading && lastVisibleItem == totalItem - 1) {
                mLoading = true;
                // Scrolled to bottom. Do something here.
                model.loadUsers(adapter.getUser(lastVisibleItem).id);
                mLoading = false;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_list);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(getTitle());

        if (findViewById(R.id.user_detail_container) != null) {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-w900dp).
            // If this view is present, then the
            // activity should be in two-pane mode.
            mTwoPane = true;
        }

        recyclerView = findViewById(R.id.user_list);
        assert recyclerView != null;

        recyclerView.setLayoutManager(mLayoutManager);
        setupRecyclerView(recyclerView, new ArrayList<>());
        recyclerView.addOnScrollListener(recyclerScrollListener);
        model = ViewModelProviders.of(this).get(UserListViewModel.class);

        model.getUsers(null).observe(this, users -> {
            adapter.appendData(users);
        });
    }

    private void setupRecyclerView(@NonNull RecyclerView recyclerView, List<User> data) {
        adapter = new SimpleItemRecyclerViewAdapter(this, data, mTwoPane);
        recyclerView.setAdapter(adapter);
    }
}
