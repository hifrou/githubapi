package pzet.pl.githubapi.data;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

import okhttp3.OkHttpClient;
import pzet.pl.githubapi.viewModel.User;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LocalService {

    private GithubService service;

    static final String BASE_URL = "https://api.github.com/";

    public LocalService() {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(new BasicAuthInterceptor("gandalfszary54@gmail.com", "mypreciousaaargh"))
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();

        service = retrofit.create(GithubService.class);
    }

    public Call<List<User>> getUsers(Integer offset){
        Call<List<User>> call = service.getUsers(offset);
        return call;
    }

}