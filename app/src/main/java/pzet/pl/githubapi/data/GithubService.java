package pzet.pl.githubapi.data;

import java.util.List;

import pzet.pl.githubapi.viewModel.User;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface GithubService {

        @GET("users")
        Call<List<User>> getUsers(@Query("since") Integer since);

}
