package pzet.pl.githubapi;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;
import pzet.pl.githubapi.viewModel.User;

public class SimpleItemRecyclerViewAdapter
        extends RecyclerView.Adapter<SimpleItemRecyclerViewAdapter.ViewHolder> {

    private final UserListActivity mParentActivity;
    public final List<User> users;
    private final boolean mTwoPane;

    private final View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            User user = (User) view.getTag();
            if (mTwoPane) {
                Bundle arguments = new Bundle();
                arguments.putInt(UserDetailFragment.ARG_ITEM_ID, user.id);
                arguments.putString(UserDetailFragment.ARG_ITEM_LOGIN, user.login);
                arguments.putString(UserDetailFragment.ARG_ITEM_AVATAR_URL, user.avatar_url);
                UserDetailFragment fragment = new UserDetailFragment();
                fragment.setArguments(arguments);
                mParentActivity.getSupportFragmentManager().beginTransaction()
                        .replace(R.id.user_detail_container, fragment)
                        .commit();
            } else {
                Context context = view.getContext();
                Intent intent = new Intent(context, UserDetailActivity.class);
                intent.putExtra(UserDetailFragment.ARG_ITEM_ID, user.id);
                intent.putExtra(UserDetailFragment.ARG_ITEM_LOGIN, user.login);
                intent.putExtra(UserDetailFragment.ARG_ITEM_AVATAR_URL, user.avatar_url);

                context.startActivity(intent);
            }
        }
    };

    public void appendData(List<User> additionalUSers){
        for (User user : additionalUSers) {
            if (users.contains(user)) {
                users.set(users.indexOf(user), user);
            } else {
                users.add(user);
            }
        }
        notifyDataSetChanged();
    }

    public User getUser(int position) {
        return users.get(position);
    }

    SimpleItemRecyclerViewAdapter(UserListActivity parent,
                                  List<User> items,
                                  boolean twoPane) {
        users = items;
        mParentActivity = parent;
        mTwoPane = twoPane;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.user_list_content, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mIdView.setText(String.valueOf(users.get(position).id));
        holder.mContentView.setText(users.get(position).login);

        holder.itemView.setTag(users.get(position));
        holder.itemView.setOnClickListener(mOnClickListener);
        if(mTwoPane && position==0) {
            holder.itemView.performClick();
        }
    }

    @Override
    public int getItemCount() {
        if(users == null){
            return 0;
        }
        return users.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        final TextView mIdView;
        final TextView mContentView;

        ViewHolder(View view) {
            super(view);
            mIdView = (TextView) view.findViewById(R.id.id_text);
            mContentView = (TextView) view.findViewById(R.id.content);
        }
    }
}
